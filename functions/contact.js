const nodemailer =  require('nodemailer');

require('dotenv').config()

exports.handler = async (event, context) => {
    const headers = {
      'Content-Type': 'application/json',
      /* Required for CORS support to work */
      'Access-Control-Allow-Origin': process.env.CLIENT_URL,
      'Access-Control-Allow-Methods': 'POST',
      'Access-Control-Allow-Headers': 'Content-Type',
      /* Required for cookies, authorization headers with HTTPS */
      // 'Access-Control-Allow-Credentials': true
    };
    const request = JSON.parse(event.body);  

    const validation = {
      name: request.name.length > 0,
      email: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(request.email),
      message: request.message.length > 6 && request.message.length < 1001
    }

    const isValid = Object.keys(validation).every(field => field);

    if(!isValid) {
      return  {
        statusCode: 400,
        headers,
        body: JSON.stringify({
          response: "Your request failed validation. Please try again in the next few moments",
        })
      }
    }

    let transporter = nodemailer.createTransport({
      host: process.env.SMTP_DOMAIN,
      port: process.env.SMTP_PORT,
      secure: true,
      auth: {
            user: process.env.SMTP_USERNAME,
            pass: process.env.SMTP_PASSWORD
      }
    })

    

    transporter.verify(function(error, success) {
      if (error) {
        return {
          statusCode: 503,
          headers,
          body: JSON.stringify({
            response: "Uh oh! It looks like the messaging service is not available. Please try again later or reach out to me personally."
          })
        }
      } else {
        console.log("Server is ready to take our messages");
      }
    });

    let info = await transporter.sendMail({
      from: `"Portfolio Contact Form" <${request.email}>`,
      to: process.env.EMAIL_TO,
      subject: request.name,
      text: request.message
    })

    return info.response == "250 Great success" ? {
      statusCode: 200,
      headers,
      body: JSON.stringify({
        response: "Your message has been successfully received!",
      })
    } : {
      statusCode: 500,
      headers,
      body: JSON.stringify({
        response: "Hmmm... something went wrong while sending your message. Try back at a later time or email me personally"
      })
    }
  }