
const api_address = process.env.DEPLOY_URL + '/.netlify/functions';

export const postContactMessage = async (data) => {
  console.log(data)
    return await fetch(`${api_address}/contact`, { 
          method: 'POST', 
          body: JSON.stringify(data), 
          headers: {
            'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
          }
        });
}