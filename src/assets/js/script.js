import { postContactMessage } from './api'

var formValidation = (function() {
  
  function formIsValid(formId) {
    const form = document.getElementById(formId);
    const isValid = form.checkValidity();
    const inputs = form.getElementsByTagName("input");
    const textareas = form.getElementsByTagName("textarea");
    const elems = [...inputs, ...textareas];

    if(!isValid) {
      elems.forEach(input => {
        if(!input.checkValidity()) {
          input.reportValidity();
        }
      })

      return false;
    } else {
      return true;
    }
  }
  
  return {
    formIsValid
  }
})()

var handling = (function() {
  function handleContactResponse(success, data) {

    let responseText = document.createElement("p");
    let responseSymbol = document.createElement("i");

    const modal = document.getElementById("contact-modal");
    const modalBody = modal.getElementsByClassName("modal-content__body");

    let responseContainer = document.createElement("div");
    responseContainer.style.cssText = "text-align: center;";

    modalBody[0].firstElementChild.hidden = true;

    responseText.style.cssText = "font-size: 2em;"
    responseSymbol.style.cssText = success ? "color: green; font-size: 2em; margin-bottom: 50px;" : "color: red; font-size: 2em; margin-bottom: 50px;"

    responseSymbol.className = success ? "fas fa-check-square" : "fas fa-exclamation-circle";
    responseText.textContent = success ? data.response : "Hmmm... something went wrong when sending your message. Please try back in the next few moments";

    responseContainer.appendChild(responseSymbol);
    responseContainer.appendChild(responseText);

    modalBody[0].appendChild(responseContainer);
  }

  return {
    handleContactResponse
  }
})()

var events = (function(handling, formValidation) {
  const navBarCollaspe = document.getElementById("navBarCollaspe");
  const collaspeEvent = new Event("click");

  const modal = document.getElementById("contact-modal");
  const btn = document.getElementById("contact-btn");
  const span = document.getElementsByClassName("close")[0];
  const scrollSpyLinks = document.getElementsByClassName("js-scroll-trigger");
  const form = document.getElementById("contact-form");
  const submitBtn = document.getElementById("contact-submit");

  const typewriteElem = document.getElementsByClassName('typewrite-sub-heading')[0];
  let data = ['Seeker of problems', 'Provider of solutions', 'Curator of binaries', 'Slinger of code'];
  let index = 0;

  
  btn.onclick = function() {
    modal.classList.remove("hide");
    modal.classList.add("show");
  };

  span.onclick = function() {
    modal.classList.remove("show")
    modal.classList.add("hide");
  };

  window.onclick = function(event) {
    if (event.target == modal) {
      modal.classList.remove("show")
      modal.classList.add("hide")
    }
  };

  submitBtn.addEventListener("click", async function(e) {
      e.preventDefault();
      
      const inputs = form.getElementsByTagName("INPUT");
      const message = form.getElementsByTagName("textarea");

      const request = {
        name: inputs.item(0).value,
        email: inputs.item(1).value,
        message: message.item(0).value
      };

      const isValid = formValidation.formIsValid("contact-form");

      if(!isValid) {
        return;
      }

      submitBtn.innerHTML = "<div class='lds-hourglass'></div>"

     postContactMessage(request).then(res => {
        if(res.ok) {
          return res.json()
        }
        else
          throw res.json();
      })
      .then(data => {
        handling.handleContactResponse(true, data);
      })
      .catch(err => {
        handling.handleContactResponse(false, err);
      })

  })

  navBarCollaspe.addEventListener("click", e => {
    const element = document.querySelector(".navbar-nav");
    let navClassName = element.className.split(" ");
    const show = navClassName.includes("show");

    if (!show) {
      navClassName = [...navClassName, "show"];
    } else {
      navClassName = navClassName.filter(elem => {
        return elem != "show";
      });
    }

    element.className = navClassName.join(" ");
  });

  for(let elem of scrollSpyLinks) {
      const href = elem.attributes["href"].value;
      const extractName = href.slice(1, href.length);

      elem.addEventListener("click", e => {
        e.preventDefault();
        document.getElementById(extractName).scrollIntoView({ behavior: "smooth" });
        navBarCollaspe.dispatchEvent(collaspeEvent);
      })
    }
    /****************************
      Typewriter Effect
    /****************************/

    typewriteElem.addEventListener('animationend', () => {
      setTimeout(() => {
        let isAnimate = typewriteElem.classList.contains('animate-typewrite');
    
        if(isAnimate) {
            typewriteElem.classList.remove('animate-typewrite');
            typewriteElem.classList.add('animate-backspace');
        } else {
            index = index + 1 < data.length ? index + 1 : 0;
            typewriteElem.innerText = data[index];
            typewriteElem.classList.remove('animate-backspace');
            typewriteElem.classList.add('animate-typewrite');
        }
    }, 2000)
    })
})(handling, formValidation);

